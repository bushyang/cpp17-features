#include <iostream>

class CData
{
public:
  int i = 1;
  float f = 2;

protected:
  double x;
};

template <size_t N>
auto& get(CData& rData) {
  if constexpr (N == 0)
    return rData.i;
  else if constexpr (N == 1)
    return rData.f;
};

namespace std
{
  template <>
  struct tuple_size<CData> : integral_constant<std::size_t, 2> {};

  template <size_t N>
  struct tuple_element<N, CData> {
    using type = decltype(get<N>(std::declval<CData&>()));;
  };
}

int main(int argc, char** argv)
{
  CData a;
  auto& [i, f] = a;

  return 0;
}