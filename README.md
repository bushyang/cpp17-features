# cpp17-features

## c++ build environment setup (optinal)
- Install Visual Studio **Build Tool** ([Build Tools for Visual Studio 2019](https://visualstudio.microsoft.com/zh-hant/downloads/#build-tools-for-visual-studio-2019))
- Install CMake

## VSCode cmake setup
* Install cmake-tool and c/c++ extension
* Select kit for project (EX: Visual Studio Community 2019 Release - x86_amd64)
* Press "build"
* Add following setting to lanuch.json 
    ```
    {
        "version": "0.2.0",
        "configurations": [
            {
                "name": "(Windows) Launch",
                "type": "cppvsdbg",
                "request": "launch",
                "program": "${command:cmake.launchTargetPath}",
                "args": [],
                "stopAtEntry": false,
                "cwd": "${workspaceFolder}",
                "environment": [],
                "console": "externalTerminal"
            }
        ]
    }
    ```
* Press F5 to start debugging