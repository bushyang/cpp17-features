cmake_minimum_required(VERSION 3.11-3.18)

project(optional)

add_executable(optional main.cpp)
target_compile_features(optional PUBLIC cxx_std_17)
